from crassum.react.bootstrap import bootstrap
from crassum.react.pyreact_functional import pyreact_sprite, use_effect, \
    use_state
from frontend.leaderboards import leaderboards
from frontend.ohrady import ohrady
from frontend.shop import shop

from frontend.toolbar import toolbar


# language=css
from frontend.upgrades import upgrades

STYLE = '''
:host {
}
'''

UPGRADES = [
    ['t-rex', 15000, 50, 1,  5],
    ['kočka', 10000, 25, 1, 10],
    ['pes',    5000, 10, 1, 30],
    ['mýval',  1000,  1, 1, 55]
]


def application(props: dict):
    kde_jsme, nastav_kde_jsme = use_state('ohrady')
    upgrades_data = localStorage.getItem('upgrades')
    if upgrades_data is None:
        localStorage.setItem('upgrades', JSON.stringify(UPGRADES))
        localStorage.setItem('jmeno', 'nikoho')
    prev_coins = localStorage.getItem('coins') or 0
    if prev_coins == 'NaN':
        localStorage.setItem('coins', 0)
    stare_coins = int(localStorage.getItem('coins') or 0)
    coins, nastav_coins = use_state(stare_coins)

    def pricti_coiny(kolik):
        celkem = coins + kolik
        nastav_coins(celkem)
        localStorage.setItem('coins', celkem)

    def zmen_zobrazeni(nove):
        nastav_kde_jsme(nove)

    komponenty = [
        (toolbar, {'coins': coins, 'zmen_zobrazeni': zmen_zobrazeni}),
    ]
    if kde_jsme == 'ohrady':
        komponenty.append((ohrady, {'pricti_coiny': pricti_coiny}))
    elif kde_jsme == 'upgrades':
        komponenty.append((upgrades, {'pricti_coiny': pricti_coiny}))
    elif kde_jsme == 'leaderboards':
        komponenty.append((leaderboards, {'pricti_coiny': pricti_coiny}))
    elif kde_jsme == 'shop':
        komponenty.append((shop, {'pricti_coiny': pricti_coiny}))

    return pyreact_sprite(
        ('div', {'class': STYLE}, komponenty)
    )


async def main():
    bootstrap(application, False)


main()
