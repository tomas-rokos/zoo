from crassum.react.pyreact_functional import pyreact_sprite, use_effect, \
    use_state

from frontend.ohrada import ohrada

# language=css
STYLE = '''
:host {
}

:host .alert {
    margin: 200px 0;
    text-align: center;
    width: 100%;
    font-weight: bold;
    font-size: 400%;
}

'''


def ohrady(props: dict):
    pricti_coiny = props['pricti_coiny']

    fences = int(localStorage.getItem('fences') or 0)

    ohrady_elems = [(ohrada, {'pricti_coiny': pricti_coiny}) for item in range(fences)]

    if len(ohrady_elems) == 0:
        ohrady_elems.append(('div', 'alert', 'Ty trubko, kup si ohradu v shopu'))
    return pyreact_sprite(
        ('div', {'class': STYLE}, ohrady_elems)
    )

