from crassum.react.pyreact_functional import pyreact_sprite, use_effect, \
    use_state


# language=css
STYLE = '''
:host {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    background-color: blue;
    color: white;
    padding: 10px;
}

:host .jmeno {
    font-weight: bold;
}
'''


def toolbar(props: dict):
    coins = props['coins']
    stare_jmeno = localStorage.getItem('jmeno')
    jmeno, nastav_jmeno = use_state(stare_jmeno)

    def zmen_jmeno():
        nove_jmeno = window.prompt('Zadej jméno', jmeno)
        nastav_jmeno(nove_jmeno)
        localStorage.setItem('jmeno', nove_jmeno)

    def zmen_zobrazeni(nove):
        def _impl():
            props['zmen_zobrazeni'](nove)
        return _impl


    return pyreact_sprite(
        ('div', {'class': STYLE}, [
            ('div', 'jmeno', [
                ('span', {'onClick': zmen_zobrazeni('ohrady')}, 'Zoo Sim - '),
                ('span', {'onClick': zmen_jmeno}, jmeno)
             ]),
            ('div', {'className': 'upgrades',
                     'onClick': zmen_zobrazeni('upgrades')}, 'upgrades'),
            ('div', 'penezenka', str(coins) + ' coins'),
            ('div', {'className': 'leaderboards',
                     'onClick': zmen_zobrazeni('leaderboards')}, 'leaderboards'),
            ('div', {'className': 'shop',
                     'onClick': zmen_zobrazeni('shop')}, 'shop'),
        ])
    )

