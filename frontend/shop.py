from crassum.react.pyreact_functional import pyreact_sprite, use_effect, use_state

# language=css
STYLE = '''
:host {
    font-weight: bold;
    font-size: 20px;
    margin: 10px;
}

:host .nadpis {
    width: 100%;
    text-align: center;
    font-size: 150%;
}

:host > .fences {
    padding: 20px;
    margin: 20px;
    border: 2px solid lightblue;
    border-radius: 5px;
    width: 200px;
}

:host > .fences .jmeno {
    font-weight: bold;
    text-align: center;
    margin: 5px;
}

:host > .fences .buy {
    font-weight: bold;
    text-align: center;
    margin: 5px;
}

:host > .fences .tlacitko {
    font-weight: bold;
    text-align: center;
    background-color: cornflowerblue;
    border-radius: 5px;
    color: white;
    margin: 5px;
    padding: 3px;
    cursor: pointer;
}


'''


def shop(props):
    prekresli, set_prekresli = use_state(0)

    fences = int(localStorage.getItem('fences') or 0)
    coins = int(localStorage.getItem('coins') or 0)
    pricti_coiny = props['pricti_coiny']

    cena = {
        0: 0,
        1: 10000,
        2: 25000,
        3: 50000,
        4: 100000,
        5: 1000000,
        6: 10000000,
        7: 100000000,
        8: 1000000000,
    }

    def _on_buy_fence():
        if coins < cena[fences]:
            alert('Málo penízků!')
            return
        pricti_coiny(-1 * cena[fences])
        localStorage.setItem('fences', fences + 1)
        set_prekresli(prekresli + 1)

    return pyreact_sprite(
        ('div', {'class': STYLE}, [
            ('div', 'nadpis', 'shop'),
            ('div', 'fences', [
                ('div', 'jmeno', ['LVL ', str(fences + 1)]),
                ('div', 'buy', 'Buy fence'),
                ('div', {'className':'tlacitko', 'onClick': _on_buy_fence},
                 'Buy ' + str(cena[fences])),
            ])
        ])
    )
