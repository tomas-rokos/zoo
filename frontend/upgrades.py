from random import random

from crassum.react.pyreact_functional import pyreact_sprite, use_effect, \
    use_state


# language=css
STYLE = '''
:host {
    display: flex;
    flex-direction: row;
}

:host > div {
    padding: 20px;
    margin: 20px;
    border: 2px solid lightblue;
    border-radius: 5px;
    width: 200px;
}

:host > div .jmeno {
    font-weight: bold;
    text-align: center;
    margin: 5px;
}

:host > div .tlacitko {
    font-weight: bold;
    text-align: center;
    background-color: cornflowerblue;
    border-radius: 5px;
    color: white;
    margin: 5px;
    padding: 3px;
    cursor: pointer;
}

:host > div .cena {
    text-align: center;
    margin: 5px;
}
'''


def upgrades(props: dict):
    prekresli, set_prekresli = use_state(0)
    moje_data = JSON.parse(localStorage.getItem('upgrades'))
    coins = int(localStorage.getItem('coins') or 0)
    pricti_coiny = props['pricti_coiny']

    def udelej_upgrade(polozka_na_upgrade):
        def _impl():
            if coins < moje_data[polozka_na_upgrade][1]:
                alert('Málo penízků!')
                return
            moje_data[polozka_na_upgrade][3] += 1
            localStorage.setItem('upgrades', JSON.stringify(moje_data))
            set_prekresli(prekresli + 1)
            pricti_coiny(-1 * moje_data[polozka_na_upgrade][1])
        return _impl

    komponenty = []
    for polozka_idx, polozka in enumerate(moje_data):
        komponenty.append(('div', None, [
            ('div', 'jmeno', [
                polozka[0], ' (', polozka[2] * polozka[3], ')',
            ]),
            ('div', {'className': 'tlacitko',
                     'onClick': udelej_upgrade(polozka_idx)}, 'Upgrade'),
            ('div', 'cena', polozka[1] + ' coins')
        ]))

    return pyreact_sprite(
        ('div', {'class': STYLE}, komponenty)
    )

