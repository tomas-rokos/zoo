from random import random

from crassum.js.array import sort_array_by_lambda
from crassum.react.pyreact_functional import pyreact_sprite, use_effect, \
    use_state


# language=css
STYLE = '''
:host {
    margin: 20px;
}

:host .gamer {
    display: flex;
    flex-direction: row;
    margin: 5px;
}

:host .gamer .poradi {
    min-width: 50px;
}

:host .gamer .jmeno {
    min-width: 200px;
}

'''

JMENA = [
    'Aatlu7845(',
    'Like8',
    'Soptre',
    'Master.IO',
    'Cupid dance',
    'Altren',
    'Ultron',
]


def leaderboards(props: dict):
    komponenty = []

    for jmeno in JMENA:
        coins = int(random() * 1000000)
        komponenty.append(('div', 'gamer', [
            ('div', 'poradi', ''),
            ('div', 'jmeno', jmeno),
            ('div', 'coiny', coins),
        ]))

    def cmp(a, b):
        if a[2][2][2] == b[2][2][2]:
            return 0
        return -1 if a[2][2][2] > b[2][2][2] else 1

    sort_array_by_lambda(komponenty, cmp)

    seat = 2
    for comp in komponenty:
        comp[2][0][2] = seat
        seat += 1

    komponenty.insert(0, ('div', 'gamer', [
        ('div', 'poradi', '1'),
        ('div', 'jmeno', 'GAUZIG CZ'),
        ('div', 'coiny', int(komponenty[0][2][2][2] * 1.22)),

    ]))

    return pyreact_sprite(
        ('div', {'class': STYLE}, komponenty)
    )

