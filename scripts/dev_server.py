#!/usr/bin/env python3
from crassum.scripts.dev_server import start_server

start_server([
    'frontend/components/*.py',
    'frontend/chart/**/*.py',
    'frontend/datastore/*.py',
    'bohemistika/**/*.py',
], 4567)
