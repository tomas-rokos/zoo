from random import random

from crassum.react.pyreact_functional import pyreact_sprite, use_effect, \
    use_state


# language=css
STYLE = '''
:host {
    display: inline-block;
    margin: 10px;
    border: 2px solid blue;
    border-radius: 5px;
    padding: 50px;
}
'''


def ohrada(props: dict):
    co_jsem, nastav_co_jsem = use_state('')
    pricti_coiny = props['pricti_coiny']

    upgrades_data = JSON.parse(localStorage.getItem('upgrades'))

    def _kliklo_se():
        hodily_jsme = random() * 100
        for upgrade in upgrades_data:
            if hodily_jsme < upgrade[4]:
                nastav_co_jsem(upgrade[0])
                pricti_coiny(upgrade[2] * upgrade[3])
                break
            hodily_jsme -= upgrade[4]

    if co_jsem == '':
        nadpis = 'tady není nic'
    else:
        nadpis = 'tady je ' + co_jsem

    return pyreact_sprite(
        ('div', {'class': STYLE, 'onClick': _kliklo_se}, [
            nadpis
        ])
    )

